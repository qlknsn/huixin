var Bige = function () {
}

Bige.prototype.tileSize = new google.maps.Size(256, 256);
//地图最大级别
Bige.prototype.maxZoom = 18;
//地图最小级别
Bige.prototype.minZoom = 12;
//地图名称
Bige.prototype.name = "基本地图";
//此地图种类获取URL的地址
Bige.prototype.getTile = function (coord, zoom, ownerDocument) {
    let img = ownerDocument.createElement("img");
    img.style.width = this.tileSize.width + "px";
    img.style.height = this.tileSize.height + "px";
    let strURL = 'http://192.168.123.232/tiles/ditu/' + zoom + '/' + coord.x + '/' + coord.y + '.png';
    img.src = strURL;
    img.onerror = function () {
        this.src = 'images/error.png';
    };
    return img;
};


var google_map = new Bige();
var Options = {
    //设置中心点
    center: new google.maps.LatLng(31.113365946637597, 121.57090365886688),
    //默认显示级别
    zoom: 18,
    panControl: false,
    zoomControl: true,
    signInControl: false,
    rotateControl: false,
    mapTypeControl: false,
    streetViewControl: false,
}
var map = new google.maps.Map(document.getElementById('mapContainer'), Options);


//像map对象中添加两种种地图
map.mapTypes.set('google', google_map);
map.setMapTypeId('google');
