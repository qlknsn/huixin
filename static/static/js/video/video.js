//根据路名进行地图搜索
let nameList = {};
$(function () {
    //标记点
    window.marks = [];
    // 选中的标记点
    window.selectedVideo = [];
    //聚合
    window.markerCluster = null;
    //右键添加摄像头的范围
    window.camsPath = null;
    //添加标记点
    addMarkers();
    //地图下拉提示
    autoPrompt();
});
//记录选中的摄像头
function noteVideoInfo(info) {
    let video = { id: info.id, ascriptionType: info.ascriptionType };
    let str = JSON.stringify(video);
    let index = window.selectedVideo.indexOf(str);
    if (index == -1) {
        window.selectedVideo.push(str);
        return 1;
    } else {
        window.selectedVideo.splice(index, 1);
        return 0;
    }
}
function showGeom(prop) {
    window.camsPath ? window.camsPath.setMap(null) : false;

    if (prop.geomType) {
        let center = {
            lat: prop.center[0].y,
            lng: prop.center[0].x,
        };
        window.camsPath = new google.maps.Circle({
            center: center,
            radius: prop.radius,
            clickable: false,
            fillColor: "blue",
            fillOpacity: "0.3",
            strokeColor: "blue",
            strokeWeight: 1,
            zIndex: 1000,
            map: this.map,
        });
    } else {
        let geom = prop.geom.map((path) => {
            return {
                lat: path.y,
                lng: path.x,
            };
        });

        window.camsPath = new google.maps.Polygon({
            path: geom,
            strokeColor: "blue",
            strokeOpacity: 1.0,
            strokeWeight: 1,
            fillColor: "blue",
            fillOpacity: 0.3,
            zIndex: 1000,
            map: window.map,
        });
    }
}
//添加标记点和聚合操作
function addMarkers() {
    $.get("/api/cam/showAllCams", {}, function (data) {
        if (data.errcode == 0) {
            let json = data.data;
            for (let i = 0; i < json.length; i++) {
                let video = {
                    id: json[i].id,
                    ascriptionType: json[i].ascriptionType,
                };
                let str = JSON.stringify(video);
                let icon = getImgSrc(json[i]);
                if (window.selectedVideo.indexOf(str) != -1) {
                    icon = "/static/img/map/selected.png";
                }
                // json[i].geomType===0?new google.maps.LatLng(json[i].geom[0].y, json[i].geom[0].x):new google.maps.LatLng(json[i].center[0].y, json[i].center[0].x),
                let position = "";
                if (json[i].geomType == 0 || !json[i].geomType) {
                    position = new google.maps.LatLng(
                        json[i].geom[0].y,
                        json[i].geom[0].x
                    );
                } else {
                    position = new google.maps.LatLng(
                        json[i].center[0].y,
                        json[i].center[0].x
                    );
                }
                let marker = new google.maps.Marker({
                    title: json[i].id.toString(),
                    icon: icon,
                    position: position,
                });
                Object.assign(marker, { prop: json[i] });
                marker.setMap(window.map);
                marker.addListener("click", function () {
                    window.noteVideoInfo(this.prop);
                    let theIcon = this.icon;
                    if (theIcon == "/static/img/map/selected.png") {
                        let src = getImgSrc(this.prop);
                        this.setIcon(src);
                    } else {
                        this.setIcon("/static/img/map/selected.png");
                    }
                });
                marker.addListener("rightclick", function () {
                    showGeom(this.prop);
                });
                window.marks.push(marker);
            }
            //点聚合
            setTimeout(() => {
                let options = {
                    gridSize: 20,
                    maxZoom: 18,
                    imagePath: "http://192.168.123.232/images/m",
                    zoomOnClick: true,
                };
                window.markerCluster = new MarkerClusterer(
                    window.map,
                    window.marks,
                    options
                );
                window.map.addListener("zoom_changed", function (e) {
                    if (this.getZoom() >= 17) {
                        window.markerCluster.setGridSize(20);
                    } else {
                        window.markerCluster.setGridSize(80);
                    }
                });
            }, 2000);
        } else if (data.errcode == -1) {
            layer.msg("后台服务器异常!", { shift: "6", time: 3000 });
        }
    });
}
//四种图片的url
// todo: 撒点图片
function getImgSrc(item) {
    let src = "/static/img/map/plot.png";
    if (item.ascriptionType == 1) {
        src = "/static/img/map/road.png";
    } else if (item.ascriptionType == 2) {
        src = "/static/img/map/face.png";
    } else if (item.ascriptionType == 3) {
        src = "/static/img/map/traffic.png";
    }
    return src;
}
//地图下拉提示
function autoPrompt() {
    $("#mapSearch").on("keyup", function () {
        let name = $.trim($(this).val());
        if (name.length <= 0) {
            return false;
        }
        $.ajax({
            type: "post",
            url: "/api/location/getLocations",
            dataType: "json",
            data: { name: name },
            success: function (response) {
                if (response.errcode == 0) {
                    nameList = response.data;
                    let arr = [];
                    for (let i = 0; i < nameList.length; i++) {
                        arr.push(nameList[i].name);
                    }
                    AutoComplete("auto_div", "mapSearch", arr);
                } else if (data.errcode == -1) {
                    layer.msg("后台服务器异常!", { shift: "6", time: 3000 });
                }
            },
            error: function (XmlHttpRequest) {
                var errorcode = XmlHttpRequest.status;
                switch (errorcode) {
                    case 401:
                        parent.layer.msg("您没有这个权限!", function () {});
                        window.top.location.reload(true);
                        break;
                    case 419:
                        parent.layer.msg("登录超时，请重新登录！", {
                            time: 1500,
                        });
                        var url = window.location.host;
                        window.top.location.href = "http://" + url + "/#/login";
                        break;
                    case 500:
                        parent.layer.msg("后台服务器异常!", function () {});
                        break;
                    default:
                        break;
                }
            },
        });
    });
}
function mpaSetCenter() {
    let name = $.trim($("#mapSearch").val());
    for (let i = 0; i < nameList.length; i++) {
        if (nameList[i].name == name) {
            let lng = nameList[i].lng;
            let lat = nameList[i].lat;
            window.map.setCenter(new google.maps.LatLng(lat, lng));
        }
    }
}
