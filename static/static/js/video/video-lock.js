/**
 * Created by Administrator on 2017/7/6 0006.
 */
let left_id_arr = JSON.parse(localStorage.getItem("leftLock")) == null ? [] : JSON.parse(localStorage.getItem("leftLock"));
let right_id_arr = JSON.parse(localStorage.getItem("rightLock")) == null ? [] : JSON.parse(localStorage.getItem("rightLock"));
$(function () {
    mouseEvent();
    sureCancel();
    //加载已锁定的视频
    loadLockedVideo();
});
//加载已锁定的视频
function loadLockedVideo() {
    $("#alertdiv table td").removeClass("lockBg");
    let left = left_id_arr;
    let right = right_id_arr;
    if (left != null) {
        for (let i = 0; i < left.length; i++) {
            $("#" + left[i]).addClass("lockBg");
        }
    }
    if (right != null) {
        for (let i = 0; i < right.length; i++) {
            $("#" + right[i]).addClass("lockBg");
        }
    }
}
//视频的锁定与解锁
function mouseEvent() {
    $("#alertdiv table td").on("mouseover", function () {
        let id = $(this).attr("id");
        if ($(this).hasClass("lockBg")) {
            $(this).addClass("unlockBg");
            $(this).on("click", {type: "unlock", id: id, obj: $(this)}, lockAndUnLock);
        } else {
            $(this).addClass("lockBg");
            $(this).on("click", {type: "lock", id: id, obj: $(this)}, lockAndUnLock);
        }
    });
    $("#alertdiv table td").on("mouseout", function () {
        if ($(this).hasClass("unlockBg")) {
            $(this).removeClass("unlockBg");
        } else if ($(this).hasClass("lockBg")) {
            $(this).removeClass("lockBg");
        }
        $(this).off("click");
    });
}
function lockAndUnLock(e) {
    let lockType = e.data.type;
    let id = e.data.id;
    let idArr = id.split("-");
    if (idArr[idArr.length-1] < 2) {
        let index  = left_id_arr.indexOf(id);
        if (index == -1) {
            left_id_arr.push(id);
        } else {
            left_id_arr.splice(index, 1);
        }
    } else {
        let index  = right_id_arr.indexOf(id);
        if (index == -1) {
            right_id_arr.push(id);
        } else {
            right_id_arr.splice(index, 1);
        }
    }
    if (lockType == "lock") {
        //锁定
        $("#" + id).addClass("unlockBg");
        e.data.obj.off("click");
        e.data.obj.on("click", {type: "unlock", id: id, obj: e.data.obj}, lockAndUnLock);
    } else {
        //解锁 lockType=="unlock"
        $("#" + id).removeClass("unlockBg");
        e.data.obj.off("click");
        e.data.obj.on("click", {type: "lock", id: id, obj: e.data.obj}, lockAndUnLock);
    }
    sendLockedIds();
}
//获取结合过uuid的mq topic地址
function getMqTopicAddress () {
    let mqTopicAddress = localStorage.getItem("mqTopicAddress");
    if (mqTopicAddress != null) {
        return mqTopicAddress;
    }
    return null;
}
function sendLockedIds() {
    let mqTopicAddress = getMqTopicAddress();
    if (mqTopicAddress == null) {
        setTimeout(sendLockedIds, 500);
        return false;
    }
    let jsonL = {type: "lock", mqType: "videoLocation", leftOrRight: "left", left: left_id_arr};
    let jsonR = {type: "lock", mqType: "videoLocation", leftOrRight: "right", right: right_id_arr};
    Stomp_Client.send(mqTopicAddress, {} , JSON.stringify(jsonR));
    Stomp_Client.send(mqTopicAddress, {} , JSON.stringify(jsonL));
    localStorage.setItem("leftLock", JSON.stringify(left_id_arr));
    localStorage.setItem("rightLock", JSON.stringify(right_id_arr));
}
//点击确定与取消
function sureCancel() {
    $("#btnSure").on("click", function () {
        left_id_arr = [];
        right_id_arr = [];
        sendLockedIds();
        loadLockedVideo();
    });
    $("#btnCancel").on("click", function () {
        var index = parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);
    });
    //鼠标按下和抬起
    $("#btnSure").on("mousedown", function () {
        $(this).css("backgroundColor", "#c5486e");
    });
    $("#btnCancel").on("mousedown", function () {
        $(this).css("backgroundColor", "#b5b5b5");
    });
    $("#btnSure").on("mouseup", function () {
        $(this).css("backgroundColor", "#FF5d8E");
    });
    $("#btnCancel").on("mouseup", function () {
        $(this).css("backgroundColor", "#DDDDDD");
    });
}
