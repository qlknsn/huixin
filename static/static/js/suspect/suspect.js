$(function () {
    loadPeopleLibs(1);
    $("#searchByLibName").on("click", searchByName);
    historyTabClick();
    $("#historySearch").on("click", historySearchFunc);
});
//获取权限
function getPermissions() {
    let permission = localStorage.getItem("userPermission");
    let perJson = JSON.parse(permission);
    if (perJson == null) {
        alert('该用户无任何权限，请更换用户！');
        var url = window.location.host;
        window.top.location.href = "http://" + url + "/#/login";
    }
    for (var i = 0; i < perJson.length; i++) {
        var permissionName = perJson[i].permissionName.split(":");
        if (permissionName[0] == "suspect") {
            if (permissionName[1] == "read") {
                $(".suspectSelect").css("display", "inline");
            } else if (permissionName[1] == "add") {
                $(".suspectAdd").css("display", "inline");
            } else if (permissionName[1] == "update") {
                $(".suspectUpdate").css("display", "inline");
            } else if (permissionName[1] == "delete") {
                $(".suspectDelete").css("display", "inline");
            }
        }
    }
}
//搜索历史记录
function historySearchFunc() {
    loadHistory(1);
}
//加载历史记录
function historyTabClick() {
    let mark = 0;
    $("#history").on("click", function () {
        if (mark == 1) {
            return false;
        }
        loadHistory(1);
        mark = 1;
    });
}
function loadHistory(pageNo) {
    let name = $.trim($("#name").val());
    let code = $.trim($("#code").val());
    let startTime = $("#logmin").val();
    let endTime = $("#logmax").val();
    let confirmStatus = $("#confirmStatus").val();
    $.ajax({
        type: "POST",
        url: "/api/suspect/faceRecognizeRecord/list",
        data: {name: name, code: code, startTime: startTime, endTime: endTime, confirmStatus: confirmStatus, pageNo: pageNo, pageSize: 8},
        dataType: "json",
        success: function (response) {
            if (response.errcode == 0) {
                let list = response.data.list;
                let html = '';
                for (let i = 0; i < list.length; i++) {
                    let gender = "未知";
                    if (format(list[i].gender == 1)) {
                        gender = "男";
                    } else if (format(list[i].gender == 2)) {
                        gender = "女";
                    }
                    let status = '<span class="label radius label-success">待确认</span>';
                    if (format(list[i].confirmStatus) == 1) {
                        status = '<span class="label radius label-danger">相同</span>';
                    } else if (format(list[i].confirmStatus) == 2) {
                        status = '<span class="label radius label-secondary">不同</span>';
                    } else if (format(list[i].confirmStatus) == 3) {
                        status = '<span class="label radius label-warning">待定</span>';
                    }
                    let ifReload = {type: true, pageNo: pageNo, confirmStatus: confirmStatus};
                    let strIfReload = JSON.stringify(ifReload);
                    html += '<tr><td class="text-c imgBigRel"><img width="100" height="100" class="picture-thumb" title="原图" src="'
                        + format(list[i].faceImageUrl) + '"><img style="margin-left: 10px;" width="100" height="100" class="picture-thumb" src="'
                        + format(list[i].faceImageUri) + '" title="抓拍"><div class="imgBigger"><img src=""></div></td>'
                        + '<td class="text-l"><table id="contentTable"><tr><td width="240">'
                        + '<p><span>姓名：</span>'+ format(list[i].name) + '</p></td><td>'
                        + '<p><span>摄像头名称：</span>' + format(list[i].faceCamName) + '</p>'
                        + '</td></tr><tr><td><p><span>性别：</span>' + gender + '</p></td><td>'
                        + '<p><span>出生年月：</span>' + format(list[i].birthday) + '</p></td></tr>'
                        + '<tr><td><p><span>证件号：</span>'+ format(list[i].personId) + '</p></td><td>'
                        + '<p><span>所属人像库：</span>'+ format(list[i].portraitName) + '</p>'
                        + '</td></tr><tr><td colspan="2"><p><span>备注：</span>'
                        + format(list[i].remarks) + '</p></td></tr></table></td>'
                        + '<td class="text-c"><p style="color: red;font-size: 16px">' + format(list[i].similarity).toFixed(2) + '%</p></td>'
                        + '<td class="text-c"><p><span>处理人员：</span>'
                        + format(list[i].handleName) + '<p><span>处理时间：</span>'
                        + format(list[i].updateTime) + '</p></td><td class="text-c">' + format(list[i].createTime) + '</td><td class="text-c">'
                        + status + '</td><td class="td-manage text-c">'
                        + '<a style="text-decoration:none" class="ml-5" '
                        + 'onClick=\'suspect_detail("查看详细","suspect-detail.html",' + format(JSON.stringify(list[i])) + ',' + strIfReload + ', 877, 510)\' href="javascript:;" title="查看详细">'
                        + '<span class="label radius label-primary">查看详细</span></a></td></tr>';
                }
                $("#historyRecords tbody").html(html);
                pageUtilHistory(response.data.totalPage, pageNo, response.data.totalCount);
                imgScaleToBig();
            } else if (response.errcode == -1) {
                layer.msg('后台服务器异常!', {shift: '6', time: 3000});
                return false;
            }
        },
        error: function (XMLHttpRequest) {
            var errorcode = XMLHttpRequest.status;
            switch (errorcode) {
                case 401:
                    layer.msg("您没有这个权限!", function () {
                    });
                    window.top.location.reload(true);
                    break;
                case 419:
                    layer.msg('登录超时，请重新登录！', {time: 1500});
                    var url = window.location.host;
                    window.top.location.href = "http://" + url + "/#/login";
                    break;
                case 500:
                    layer.msg("后台服务器异常!", function () {
                    });
                    break;
                default:
                    break;
            }
        }
    })
}
// 图片经过放大
function imgScaleToBig() {
    $(".imgBigRel .picture-thumb").hover(function () {
        let src = $(this).attr("src");
        let screenHeight = $(document.body).height();
        let windowHeight = $(window).height();
        let height = screenHeight;
        if (screenHeight < windowHeight) {
            height = windowHeight;
        }
        let top = $(this).parents(".imgBigRel").offset().top;
        let tdHeight = $(this).parents(".imgBigRel").height();
        let bigHeight = $(this).parents(".imgBigRel").find(".imgBigger").height();
        let allHeight = top + tdHeight + bigHeight;
        if (allHeight > height && top > bigHeight) {
            $(this).parents(".imgBigRel").find(".imgBigger").css({
                display: "block",
                bottom: "100%"
            }).find("img").attr("src", src);
        } else {
            $(this).parents(".imgBigRel").find(".imgBigger").css({
                display: "block",
                top: "100%"
            }).find("img").attr("src", src);
        }
    }, function () {
        $(this).parents(".imgBigRel").find(".imgBigger").css("display", "none");
    })
}

//点击搜索
function searchByName() {
    loadPeopleLibs(1);
}
//加载从脸库
function loadPeopleLibs(pageNo) {
    let name = $.trim($("#libName").val());
    $("#addNewPeopleLib").off("click");
    $("#addNewPeopleLib").on("click", function () {
        lib_add('新增人群库', 'suspect-lib-add.html?pageNo=' + pageNo, 770, 350)
    });
    $.ajax({
        type: "POST",
        url: "/api/suspect/portrait/list",
        data: {name: name, pageNo: pageNo, pageSize: 10},
        dataType: "json",
        success: function (response) {
            if (response.errcode == 0) {
                let list = response.data.list;
                let html = "";
                let isRename = {pageNo: pageNo};
                let strIsRename = JSON.stringify(isRename);
                $("#count").text(response.data.suspectNum);
                for (let i = 0; i < list.length; i++) {
                    let json = {id: format(list[i].id), name: format(list[i].name), comment: format(list[i].comment)};
                    let str = JSON.stringify(json);

                    html += '<tr class="text-c"><td>' + format(list[i].name) + '</td><td>' + format(list[i].suspectNum) + '</td><td>'
                    + format(list[i].comment) + '</td><td>' + format(list[i].createTime) + '</td><td>'
                    + format(list[i].updateTime) + '</td><td>'
                    + '<span value="' + format(list[i].id) + '" style="display: none;" class="label label-primary radius edit suspectSelect"  '
                    + 'onclick="system_category_lookup(\'' + format(list[i].name) + '\',\'suspect-people-imgs-lookup.html\',' + format(list[i].id) + ',1290, 730)">查看</span>'
                    + '<span style="display: none;" class="label label-success radius edit suspectAdd" onclick="lib_people_add(\'人像添加\',\'suspect-people-add.html?pageNo=' + pageNo + '\',' + format(list[i].id) + ',770,580)">添加人像</span>'
                    + '<span style="display: none;" class="label label-primary radius edit rename suspectUpdate" '
                    + 'onclick=\'lib_edit("重命名人群库", "suspect-lib-edit.html", ' + str + ',' + strIsRename + ', 770, 350)\'>重命名</span>'
                    + '<span style="display: none;" class="label label-success radius edit suspectDelete" onclick="system_peoplelib_del(this, ' +  format(list[i].id) + ',' + pageNo + ')">删除</span></td></tr>';
                }
                $("#peopleAllLibs tbody").html(html);
                pageUtil(response.data.totalPage, pageNo, response.data.totalCount)
                //获取权限
                getPermissions();
            } else if (response.errcode == -1) {
                layer.msg('后台服务器异常!', {shift: '6', time: 3000});
                return false;
            }
        },
        error: function (XMLHttpRequest) {
            var errorcode = XMLHttpRequest.status;
            switch (errorcode) {
                case 401:
                    layer.msg("您没有这个权限!", function () {
                    });
                    window.top.location.reload(true);
                    break;
                case 419:
                    layer.msg('登录超时，请重新登录！', {time: 1500});
                    var url = window.location.host;
                    window.top.location.href = "http://" + url + "/#/login";
                    break;
                case 500:
                    layer.msg("后台服务器异常!", function () {
                    });
                    break;
                default:
                    break;
            }
        }
    })
}
function pageUtilHistory(totalPage, currentPage, count) {
    $("#tcdPageHistory").html("");
    $("#tcdPageHistory").createPage({
        pageCount: totalPage,
        current: currentPage,
        count: count,
        backFn: function (p) {
            loadHistory(p);
        }
    });
}
function pageUtil(totalPage, currentPage, count) {
    $("#peopleAllLibsList").createPage({
        pageCount: totalPage,
        current: currentPage,
        count: count,
        backFn: function (p) {
            loadPeopleLibs(p);
        }
    });
}
function format(data) {
    if (data === undefined) {
        return "";
    } else {
        return data;
    }
}
