var message_indicate = 0;
var hover_time = 0;
$(function () {
    // messageFlicker();
    $("#messages").on("click", messageClick);
    msgInfoMouseOver();
    $("#messages").css("backgroundImage", "url('img/msginfo.png')");
});
//点击消息
function messageClick() {
    message_indicate = 0;
}
//消息列表一些效果
function messageListMouseEvent() {
    $("#messageListDiv .contentDiv").hover(function () {
        $(this).css("backgroundColor", "#ddeff0");
        $(this).find(".deleteImgDiv img").css("visibility", "visible");
        if ($(this).find(".peopel")) {
            $(this).find(".people img").attr("src", "img/people2.png");
        }
        if ($(this).find(".car")) {
            $(this).find(".car img").attr("src", "img/car2.png");
        }
    }, function () {
        $(this).css("backgroundColor", "#f5f5f5");
        $(this).find(".deleteImgDiv img").css("visibility", "hidden");
        if ($(this).find(".peopel")) {
            $(this).find(".people img").attr("src", "img/people1.png");
        }
        if ($(this).find(".car")) {
            $(this).find(".car img").attr("src", "img/car1.png");
        }
    });
}
//接收消息推送
// function receiveMsg() {
//     var socket = new SockJS(SOCKET_ADDRESS);
//     var stompClient = Stomp.over(socket);
//     stompClient.debug = function (str) {
//         // console.log(str)
//     };
//     var on_connect = function () {
//         let permission = localStorage.getItem("userPermission");
//         let perJson = JSON.parse(permission);
//         if (perJson == null) {
//             alert('该用户无任何权限，请更换用户！');
//             var url = window.location.host;
//             window.top.location.href = "http://" + url + "/#/login";
//         }
//         for (let m = 0; m < perJson.length; m++) {
//             if (perJson[m].permissionName == "suspect:read") {
//                 //人脸权限
//                 stompClient.subscribe('/topic/getFaceWarning', function (response) {
//                     document.getElementById("audioMessage").play();
//                     let data = JSON.parse(response.body);
//                     if (message_indicate == 0) {
//                         message_indicate = 1;
//                         messageFlicker();
//                     }
//                     if (data.errcode == 0) {
//                         let json = data.data;
//                         let html = '';
//                         let ifReload = {type: false};
//                         let ifReloadStr = JSON.stringify(ifReload);
//                         for (let i = 0; i < json.similarFaceVOS.length; i++) {
//                             json.similarFaceVOS[i].faceImageUri = json.faceImageUri;
//                             json.similarFaceVOS[i].pictureUri = json.pictureUri;
//                             json.similarFaceVOS[i].timestampBegin = json.timestampBegin;
//                             json.similarFaceVOS[i].faceCamName = json.faceCamName;
//                             json.similarFaceVOS[i].id = json.similarFaceVOS[i].faceRecognizeRecordId;
//                             let jsonStr = JSON.stringify(format(json.similarFaceVOS[i]));
//                             let right = 30 * i;
//                             html += '<div class="alertPrimaryPeople" style="right: ' + right + 'px"><p class="header"><span class="title">监测弹窗'
//                                 + '</span> <span class="closeX" onclick="closeAlert(this)">X</span>'
//                                 + '</p><div class="contentBody"><div class="imgDiv" '
//                                 + 'onclick=\'suspect_detail("查看详细","html/suspect/suspect-detail.html",' + jsonStr + ',' + ifReloadStr + ',887,510)\'>'
//                                 + '<img onload="autoCloseAlert(this)" src="' + format(json.faceImageUri) + '"></div><div class="info" '
//                                 + 'onclick=\'suspect_detail("查看详细","html/suspect/suspect-detail.html",' + jsonStr + ',' + ifReloadStr + ',887,510)\'>'
//                                 + '<h3>重点人群监测</h3><p><span>相似度：</span>' + format(json.similarFaceVOS[i].similarity).toFixed(2)
//                                 + '%</p><p><span>所属人像库：</span>' + format(json.similarFaceVOS[i].portraitName) + '</p><p><span>姓名：</span>'
//                                 + format(json.similarFaceVOS[i].name) + '</p><p><span>地点：</span>' + format(json.faceCamName)
//                                 + '</p></div></div></div>';
//                         }
//                         $("#alertReceiveMsg").html(html);
//                         hideAlertAndClose(1);
//                     }
//                 });
//             }
//             if (perJson[m].permissionName == "illegalParking:read") {
//                 //车辆权限
//                 stompClient.subscribe('/topic/getCarWarning', function (response) {
//                     document.getElementById("audioMessage").play();
//                     let json = JSON.parse(response.body);
//                     if (message_indicate == 0) {
//                         message_indicate = 1;
//                         messageFlicker();
//                     }
//                     let violationType = format(json.violationType);
//                     violationType = (violationType == 7002) ? "车辆违停" : format(json.violationType);
//                     let html = '';
//                     let ifReload = {type: false};
//                     let ifReloadStr = JSON.stringify(ifReload);
//                     let jsonStr = JSON.stringify(json);
//                     html += '<div class="alertPrimaryPeople" style="right: 15px"><p class="header"><span class="title">监测弹窗'
//                         + '</span> <span class="closeX" onclick="closeAlert(this)">X</span>'
//                         + '</p><div class="contentBody"><div class="imgDiv" '
//                         + 'onclick=\'car_detail("查看详细","html/car/car-detail.html",' + jsonStr + ',' + ifReloadStr + ',887,487)\'>'
//                         + '<img onload="autoCloseAlert(this)" src="' + format(json.carNumberImgUrl) + '"></div><div class="info" '
//                         + 'onclick=\'car_detail("查看详细","html/car/car-detail.html",' + jsonStr + ',' + ifReloadStr + ',887,487)\'>'
//                         + '<h3>车辆违停监测</h3><p><span>车牌号：</span>' + format(json.carNumber)
//                         + '</p><p><span>摄像头名称：</span>' + format(json.camName) + '</p><p><span>类型：</span>'
//                         + violationType + '</p>'
//                         + '</div></div></div>';
//                     $("#alertReceiveMsg").html(html);
//                     hideAlertAndClose(2);
//                 });
//             }
//             if (perJson[m].permissionName == "mainControl:read") {
//                 //主控权限
//                 let sceneTime = JSON.parse(localStorage.getItem("sceneAndTime"));
//                 if (sceneTime != null) {
//                     let html = '<span>场景名称：</span>' + sceneTime.scene;
//                     $("#sceneVideoLoop").html(html);
//                 }
//                 $("#lockVideo").css("display", "inline");
//                 stompClient.subscribe('/topic/getVideoLoopTask', function (response) {
//                     let json = JSON.parse(response.body);
//                     let html = '<span>场景名称：</span>' + json.scene;
//                     $("#sceneVideoLoop").html(html);
//                     let sceneAndTime = {scene: json.scene, cron: json.cron};
//                     localStorage.setItem("sceneAndTime", JSON.stringify(sceneAndTime));
//                 });
//             }
//         }
//     };
//     var on_error = function () {
//         layer.msg("socket连接时发生了错误!", {shift: '6', time: 3000});
//         stompClient.disconnect();
//         setTimeout(function () {
//             socket = new SockJS(SOCKET_ADDRESS);
//             stompClient = Stomp.over(socket);
//             stompClient.connect({}, on_connect, on_error);
//         }, 120000);
//     };
//     stompClient.connect({}, on_connect, on_error);
// }
//点击弹窗查看详细时隐藏弹窗
function hideAlertAndClose(type) {
    //type: 0：点击消息列表；  1：忽略人脸；   2：忽略停车
    if (type == 0) {
        $("#messageListDiv .contentDiv .headImgDiv img").off("click");
        $("#messageListDiv .contentDiv td.describe").off("click");
        $("#messageListDiv .contentDiv .headImgDiv img").on("click", function () {
            let typeAndId = $(this).attr("value");
            let json = JSON.parse(typeAndId);
            ignoreAlertOrList(json.type, json.id, this);
            $("#messageListDiv").css("display", "none");
        });
        $("#messageListDiv .contentDiv td.describe").on("click", function () {
            let typeAndId = $(this).attr("value");
            let json = JSON.parse(typeAndId);
            ignoreAlertOrList(json.type, json.id, this);
            $("#messageListDiv").css("display", "none");
        });
    } else {
        $(".alertPrimaryPeople .imgDiv").off("click");
        $(".alertPrimaryPeople .info").off("click");

        $(".alertPrimaryPeople .imgDiv").on("click", function () {
            $(this).parents(".alertPrimaryPeople").remove();
        });
        $(".alertPrimaryPeople .info").on("click", function () {
            $(this).parents(".alertPrimaryPeople").remove();
        });
    }
}
//隐藏推送弹窗
function closeAlert(obj) {
    $(obj).parents(".alertPrimaryPeople").fadeOut(200);
    setTimeout(function () {
        $(obj).parents(".alertPrimaryPeople").remove();
    }, 200);
}
//推送弹窗自动隐藏
function autoCloseAlert(obj) {
    setTimeout(function () {
        closeAlert(obj);
    }, 20000);
}
//鼠标滑过或点击消息时，弹出消息列表
function msgInfoMouseOver() {
    $("#messageLi").hover(function () {
        message_indicate = 0;
        $("#messageListDiv").css("display", "block");
        let currentDate = new Date().getTime();
        let time = (currentDate - hover_time) / 1000;
        hover_time = currentDate;
        if (time > 2) {
            $.ajax({
                type: "GET",
                url: "/api/suspect/faceRecognizeRecord/getUnsolvedPush",
                dataType: "json",
                success: function (response) {
                    if (response.errcode == 0) {
                        $("#messageListDiv .contentList").mCustomScrollbar("destroy");
                        let list = response.data.list;
                        $("#msgListCount").text(response.data.totalCount);
                        let ifReload = {type: false};
                        let ifReloadStr = JSON.stringify(ifReload);
                        let html = '';
                        for (let i = 0; i < list.length; i++) {
                            if (list[i].type == 0) {
                                let typeAndId = {type: 1, id: format(list[i].id)};
                                html += '<div class="contentDiv"><table><tr><td class="listIndex people"><div class="listTypeImgDiv">' +
                                    '<img src="img/people1.png"></div></td><td class="headImgDiv"><div>' +
                                    '<img onclick=\'suspect_detail("查看详细","html/suspect/suspect-detail.html",' + JSON.stringify(format(list[i])) + ',' + ifReloadStr + ',887,510)\'' +
                                    ' src="' + format(list[i].faceImageUri) + '" value=\'' + JSON.stringify(typeAndId) + '\'></div></td><td class="describe" ' +
                                    ' onclick=\'suspect_detail("查看详细","html/suspect/suspect-detail.html",' + JSON.stringify(format(list[i])) + ',' + ifReloadStr + ',887,510)\'' +
                                    ' value=\'' + JSON.stringify(typeAndId) + '\'>' +
                                    '<p>' + format(list[i].portraitName) + '</p>';
                                if (format(list[i].name) != "") {
                                    html += '<p><span>姓名：</span>' + format(list[i].name) + '</p>';
                                } else if (format(list[i].code) != "") {
                                    html += '<p><span>代号：</span>' + format(list[i].code) + '</p>';
                                }
                                html += '<p>' + format(list[i].suspectCreateTime) + '</p></td>' +
                                    '<td class="deleteImgDiv"><div><img title="忽略" onclick="ignoreAlertOrList(1,' + format(list[i].id) + ',this)" src="img/delete_icon_nor.png">' +
                                    '</div></td></tr></table></div>';
                            } else if (list[i].type == 1) {
                                let typeAndId = {type: 2, id: format(list[i].id)};
                                html += '<div class="contentDiv"><table><tr><td class="listIndex car"><div class="listTypeImgDiv">' +
                                    '<img src="img/car1.png"></div></td><td class="headImgDiv"><div>' +
                                    '<img onclick=\'car_detail("查看详细","html/car/car-detail.html",' + JSON.stringify(format(list[i])) + ',' + ifReloadStr + ',887,487)\'' +
                                    ' src="' + format(list[i].carNumberImgUrl) + '" value=\'' + JSON.stringify(typeAndId) + '\'></div></td><td class="describe" ' +
                                    ' onclick=\'car_detail("查看详细","html/car/car-detail.html",' + JSON.stringify(format(list[i])) + ',' + ifReloadStr + ',887,487)\'' +
                                    ' value=\'' + JSON.stringify(typeAndId) + '\'>' +
                                    '<p><span>摄像头：</span>' + format(list[i].camName) + '</p><p><span>车牌号：</span>' + format(list[i].carNumber) + '</p>' +
                                    '<p>' + format(list[i].createTime) + '</p></td>' +
                                    '<td class="deleteImgDiv"><div><img title="忽略" onclick="ignoreAlertOrList(2,' + format(list[i].id) + ',this)" src="img/delete_icon_nor.png">' +
                                    '</div></td></tr></table></div>';
                            }
                        }
                        $("#messageListDiv .contentList").html(html);

                        $("#messageListDiv .contentList").mCustomScrollbar({
                            mouseWheel:true,
                        });
                        messageListMouseEvent();
                        ignoreIconHover();
                        hideAlertAndClose(0);
                    } else if (response.errcode == -1) {
                        layer.msg("后台服务器异常!", {shift: '6', time: 3000});
                    }
                },
                error: function (XmlHttpRequest) {
                    var errorcode = XmlHttpRequest.status;
                    switch (errorcode) {
                        case 401:
                            parent.layer.msg("您没有这个权限!", function () {
                            });
                            location.reload(true);
                            break;
                        case 419:
                            parent.layer.msg('登录超时，请重新登录！', {time: 1500});
                            var url = window.location.host;
                            window.top.location.href = "http://" + url + "/#/login";
                            break;
                        case 500:
                            parent.layer.msg("后台服务器异常!", function () {
                            });
                            break;
                        default:
                            break;
                    }
                }
            })
        }
    }, function () {
        $("#messageListDiv").css("display", "none");
        let currentDate = new Date().getTime();
        hover_time = currentDate;
    });
    $("#messages").on("click", function () {
        $("#messageListDiv").css("display", "block");
    });
}
function ignoreIconHover() {
    $(".deleteImgDiv img").hover(function () {
        $(this).attr("src", "img/delete_icon_sel.png");
    }, function () {
        $(this).attr("src", "img/delete_icon_nor.png");
    })
}
//点击忽略
function ignoreAlertOrList(type, id, obj) {
    //type:忽略推送的类型（0:全部忽略 1：人脸推送 2：违章车辆推送）
    $.ajax({
        type: "POST",
        url: "/api/suspect/faceRecognizeRecord/setIgnore",
        dataType: "json",
        data: {type: type, id: id},
        success: function (response) {
            if (response.errcode == 0) {
                if (type == 0) {
                    $("#messageListDiv .contentList").html("");
                    $("#msgListCount").text(0);
                } else {
                    $(obj).parents(".contentDiv").remove();
                    $("#msgListCount").text($("#msgListCount").text()-1);
                }
            } else if (response.errcode == -1) {
                layer.msg("后台服务器异常!", {shift: '6', time: 3000});
            }
        },
        error: function (XmlHttpRequest) {
            var errorcode = XmlHttpRequest.status;
            switch (errorcode) {
                case 401:
                    parent.layer.msg("您没有这个权限!", function () {
                    });
                    location.reload(true);
                    break;
                case 419:
                    parent.layer.msg('登录超时，请重新登录！', {time: 1500});
                    var url = window.location.host;
                    window.top.location.href = "http://" + url + "/#/login";
                    break;
                case 500:
                    parent.layer.msg("后台服务器异常!", function () {
                    });
                    break;
                default:
                    break;
            }
        }
    })
}
//消息跳动提示 1:闪烁   0：停止闪烁
function messageFlicker() {
    if (message_indicate == 1) {
        setTimeout(messageFlicker, 1000);
        setTimeout(function () {
            $("#messages").css("visibility", "hidden");
        }, 500);
        setTimeout(function () {
            $("#messages").css("visibility", "visible");
        }, 1000);
    }
}
function format(data) {
    if (data === undefined) {
        return "";
    } else {
        return data;
    }
}
