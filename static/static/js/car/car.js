$(function () {
    carRecordList(1);
});

function carRecordList(pageNo) {
    let startTime = $.trim($("#startTime").val());
    let endTime = $.trim($("#endTime").val());
    let carNumber = $.trim($("#carNumber").val());
    let state = $("#state").val();
    $.ajax({
        type: "post",
        url: "/api/car/listRecord",
        dataType: "json",
        data: {carNumber: carNumber, startTime: startTime, endTime: endTime, state: state, pageNo: pageNo, pageSize: 10},
        success: function (d) {
            if (d.errcode == 0) {
                $("#count").html(d.data.totalCount);
                let carRecord = d.data.list;
                let ifReload = {type: true, pageNo: pageNo, state: state};
                let html = "";
                for (let i = 0; i < carRecord.length; i++) {
                    let unStop = carRecord[i].violationType;
                    if (carRecord[i].violationType == 7002) {
                        unStop = "违章停车";
                    }
                    let states = '<span class="label radius label-success">待确认</span>';
                    if (carRecord[i].state == 1) {
                        states = '<span class="label radius label-danger">违章</span>';
                    } else if (carRecord[i].state == 2) {
                        states = '<span class="label radius label-secondary">没有违章</span>';
                    } else if (carRecord[i].state == 3) {
                        states = '<span class="label radius label-warning">待定</span>';
                    }
                    html += '<tr class="text-c">' +
                        '<td class="imgBigRel"><a href="javascript:;"><img width="210" class="picture-thumb" src="' + format(carRecord[i].carNumberImgUrl) + '"></a>' +
                        '<div class="imgBigger"><img src="' + format(carRecord[i].carNumberImgUrl) + '"></div></td>' +
                        '<td>' + format(carRecord[i].carNumber) + '</td>' +
                        '<td>' + format(carRecord[i].startTime) + '</td>' +
                        '<td>' + format(carRecord[i].createTime) + '</td>' +
                        '<td>' + unStop + '</td>' +
                        '<td><p><span>处理人员：</span>' + format(carRecord[i].userName) + '</p>' +
                        '<p><span>处理时间：</span>' + format(carRecord[i].updateTime) + '</p></td>' +
                        '<td>' + states + '</td>' +
                        '<td class=td-manage>' +
                        '<a title="查看详细" href="javascript:;" onclick=\'record_edit("违章停车详细信息","car-detail.html",' + JSON.stringify(carRecord[i]) + ',' + JSON.stringify(ifReload) + ',877,487)\' class="ml-5" style="text-decoration:none">' +
                        '<span class="label radius label-primary">查看详细</span></a></td></tr>';

                }
                $("#carlist tbody").html(html);
                imgScaleToBig();
                pageUtil(d.data.totalPage, pageNo, d.data.totalCount);
            } else if (d.errcode == -1) {
                layer.msg('后台服务器异常!', {time: 1500});
            }
        },
        error: function (XmlHttpRequest) {
            var errorcode = XmlHttpRequest.status;
            switch (errorcode) {
                case 401:
                    parent.layer.msg("您没有这个权限!", function () {
                    });
                    window.top.location.reload(true);
                    break;
                case 419:
                    parent.layer.msg('登录超时，请重新登录！', {time: 1500});
                    var url = window.location.host;
                    window.top.location.href = "http://" + url + "/#/login";
                    break;
                case 500:
                    parent.layer.msg("后台服务器异常!", function () {
                    });
                    break;
                default:
                    break;
            }
        }
    })
}
// 图片经过放大
function imgScaleToBig() {
    $(".imgBigRel .picture-thumb").hover(function () {
        //
        let screenHeight = $(document.body).height();
        let windowHeight = $(window).height();
        let height = screenHeight;
        if (screenHeight < windowHeight) {
            height = windowHeight;
        }
        let top = $(this).parents(".imgBigRel").offset().top;
        let tdHeight = $(this).parents(".imgBigRel").height();
        let bigHeight = $(this).parents(".imgBigRel").find(".imgBigger").height();
        let allHeight = top + tdHeight + bigHeight;
        if (allHeight > height && top > bigHeight) {
            $(this).parents(".imgBigRel").find(".imgBigger").css({
                display: "block",
                bottom: "100%"
            });
        } else {
            $(this).parents(".imgBigRel").find(".imgBigger").css({
                display: "block",
                top: "100%"
            });
        }
    }, function () {
        $(this).parents(".imgBigRel").find(".imgBigger").css("display", "none");
    })
}
function pageUtil(totalPage, currentPage, count) {
    $(".tcdPageCode").createPage({
        pageCount: totalPage,
        current: currentPage,
        count: count,
        backFn: function (p) {
            carRecordList(p);
        }
    });
}

function format(data) {
    if (data === undefined) {
        return "";
    } else {
        return data;
    }
}
