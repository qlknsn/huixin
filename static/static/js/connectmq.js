var Stomp_Client = "";
function connectMq() {
    
    var ws;
    if (location.search == "?ws") {
        ws = new WebSocket(WEBSOCKET_ADDRESS)
    } else {
        ws = new SockJS(SOCKJS_ADDRESS);
    }
    // 获得Stomp client对象
    Stomp_Client = Stomp.over(ws);
    Stomp_Client.debug = function (str) {
        // console.log(str)
    };
    Stomp_Client.heartbeat.outgoing = 0;
    Stomp_Client.heartbeat.incoming = 0;
    // 定义连接成功回调函数
    function on_connect() {
//      client.send("/topic/testTopic", {}, data);
    };
    // 定义错误时回调函数
    var on_error = function () {
        // parent.layer.msg("mq连接时发生了错误!", {shift: '6', time: 3000});
        Stomp_Client.disconnect();
        setTimeout(connectMq, 120000);
    };
    // 连接RabbitMQ
    Stomp_Client.connect(MQ_USERNAME, MQ_PASSWORD, on_connect, on_error, '/');
}
connectMq();
