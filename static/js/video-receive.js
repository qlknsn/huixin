function addBorderById(data, left) {
    if (left) {
        for (let i = 0; i < 6; i++) {
            for (let j = 0; j < 2; j++) {
                let id = "grid-" + i + "-" + j;
                $("#" + id).removeClass("addLockBorder");
            }
        }
    } else {
        for (let i = 0; i < 6; i++) {
            for (let j = 2; j < 4; j++) {
                let id = "grid-" + i + "-" + j;
                $("#" + id).removeClass("addLockBorder");
            }
        }
    }
    for (let i = 0; i < data.length; i++) {
        $("#" + data[i]).addClass("addLockBorder");
    }
}

function loadVideoByIdAndUrl(id, url, type) {
    // let str = "<object type='application/x-vlc-plugin' pluginspage='http://www.videolan.org/' id='vlc" + id + "' events='false' style='width: 100%;height: 100%;'>";
    // str += "<param name='mrl' value='" + url + "'/>";
    // str += "<param name='volume' value='50'/><param name='autoplay' value='true'/>";
    // str += "<param name='loop' value='false'/><param name='fullscreen' value='true'/>";
    // str += "<param name='controls' value='false'/><param name='toolbar' value='false' /></object>";


    $("#" + id +">object>param[name='mrl']").val(url);
    let vlc = document.getElementById(id).getElementsByTagName("object")[0];
    let itemId = vlc.playlist.add(url);
    vlc.playlist.playItem(itemId);

    if (type == "left") {
        getVideoUrlsLeft();
    } else {
        getVideoUrlsRight();
    }
}

function videoCircle(arrId, urls, type) {
    for (let i = 0; (i < arrId.length) && (i < urls.length); i++) {
        // let str = "<object type='application/x-vlc-plugin' pluginspage='http://www.videolan.org/' id='vlc" + arrId[i] + "' events='false' style='width: 100%;height: 100%;'>";
        // str += "<param name='mrl' value='" + urls[i] + "'/>";
        // str += "<param name='volume' value='50'/><param name='autoplay' value='true'/>";
        // str += "<param name='loop' value='false'/><param name='fullscreen' value='true'/>";
        // str += "<param name='controls' value='false'/><param name='toolbar' value='false' /></object>";
        // $("#" + arrId[i]).html(str);

        $("#" + arrId[i] +">object>param[name='mrl']").val(urls[i]);
        let vlc = document.getElementById(arrId[i]).getElementsByTagName("object")[0];
        let itemId = vlc.playlist.add(urls[i]);
        vlc.playlist.playItem(itemId);

    }
    if (type == "left") {
        getVideoUrlsLeft();
    } else {
        getVideoUrlsRight();
    }
}

//获取结合过uuid的mq topic地址
function getMqTopicAddress () {
    let mqTopicAddress = localStorage.getItem("mqTopicAddress");
    if (mqTopicAddress != null) {
        return mqTopicAddress;
    }
    return null;
}

//vlc各种事件
function registerVLCEvent(event, handler) {
    // let vlc = document.getElementById(id);
    //     if (vlc) {
    //         if (vlc.attachEvent) {
    //             // Microsoft
    //             vlc.attachEvent (event, handler.bind(vlc));
    //         } else if (vlc.addEventListener) {
    //             // Mozilla: DOM level 2
    //             vlc.addEventListener (event, handler.bind(vlc), false);
    //         }
    //     }
    let vlc = document.getElementsByClassName("videoObject");
    for (let i = 0; i < vlc.length; i++) {
        if (vlc[i]) {
            if (vlc[i].attachEvent) {
                // Microsoft
                vlc[i].attachEvent (event, handler.bind(vlc[i]));
            } else if (vlc[i].addEventListener) {
                // Mozilla: DOM level 2
                vlc[i].addEventListener (event, handler.bind(vlc[i]), false);
            }
        }
    }
}
// stop listening to event
function unregisterVLCEvent(event, handler) {
    let vlc = document.getElementsByClassName("videoObject");
    for (let i = 0; i < vlc.length; i++) {
        if (vlc[i]) {
            if (vlc[i].detachEvent) {
                // Microsoft
                vlc[i].detachEvent (event, handler);
            } else if (vlc[i].removeEventListener) {
                // Mozilla: DOM level 2
                vlc[i].removeEventListener (event, handler, false);
            }
        }
    }
}
function handle_MediaPlayerEncounteredError(){
    let id = this.parentElement.id;
    loadVideoError(id);
}
function handle_MediaPlayerStopped(){
    // console.log(this)
    // let id = this.parentElement.id;
    // loadVideoError(id);
}

$(function () {
    // Register a bunch of callbacks.
    registerVLCEvent("MediaPlayerEncounteredError", handle_MediaPlayerEncounteredError);
    registerVLCEvent("MediaPlayerPaused", handle_MediaPlayerStopped);
});
