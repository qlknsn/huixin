/**
 * Created by Administrator on 2017/7/4 0004.
 */


var Bige = function () {

    //地图最小级别
    minZoom = 12;
    //地图名称
    name = "基本地图";
    //此地图种类获取URL的地址
    getTile = (coord, zoom, ownerDocument) => {
        let img = ownerDocument.createElement("img");
        img.style.width = this.tileSize.width + "px";
        img.style.height = this.tileSize.height + "px";
        // let strURL='http://mt0.google.cn/vt/lyrs=m@365000000&hl=zh-CN&gl=cn&x=' + coord.x + '&y=' +coord.y + '&z=' + zoom + '&s='+'Galileo'.substring(0,coord.x%8);
        let strURL = 'http://192.168.123.232/tiles/ditu/' + zoom + '/' + coord.x + '/' + coord.y + '.png';
        img.src = strURL;
        img.onerror = function () {
            this.src = 'images/error.png';
        };
        return img;
    };
};
Bige.prototype.tileSize = new google.maps.Size(256, 256);
//地图最大级别
Bige.prototype.maxZoom = 18;

