/**
 * Created by Administrator on 2017/7/4 0004.
 */
import axios from "axios";

let base = "/api";

//添加设置
let axios1 = axios.create({
    transformRequest: [
        function (data) {
            let st = "";
            for (let i in data) {
                let str = "";
                str = `${i}=${data[i]}&`;
                st += str;
            }
            //依自己的需求对请求数据进行处理
            return st;
        },
    ],
    timeout: 300000,
});

// 添加响应拦截器
axios1.interceptors.response.use(
    function (response) {
        // 对响应数据做点什么
        return response;
    },
    function (error) {
        // 对响应错误做点什么
        if (error.response.status === 401) {
            return Promise.reject("权限不足");
        }
        if (error.response.status === 419) {
            localStorage.removeItem("user");
            window.location.href = "http://" + window.location.host + "#/login"; //negix 外网服务器地址
            // window.location.href = 'https://' + window.location.host + '#/login' //negix 外网服务器地址
        } else {
            return Promise.reject("服务器问题");
        }
    }
);

//登陆，登出
export const reqLogin = (params) => {
    return axios1.post(`${base}/user/login`, params).then((res) => res.data);
};
export const reqLogout = (params) => {
    return axios1
        .get(`${base}/user/logout`, { params: params })
        .then((res) => res.data);
};
//请求摄像头列表
export const reqCamslist = (params) => {
    return axios1
        .get(`${base}/cam/showCamsByPosition`, { params: params })
        .then((res) => res.data);
};
//地理位置
export const reqgetLocations = (params) => {
    return axios1
        .get(`http://restapi.amap.com/v3/place/text`, {
            params: Object.assign(params, {
                key: "6411cc8677263b198eb56f7ea8de7b06",
            }),
        })
        .then((res) => res.data);
};
//所有摄像头
export const reqAllcams = (params) => {
    return axios1
        .get(`${base}/cam/showAllCams/v2`, { params: params })
        .then((res) =>{
            console.log(res.data)
            return res.data
        } );
};
// 获取海康平台直播流地址
export const hkVideo = (params) => {
    return axios1
        .get(
            `${base}/cam/cameraPreviewURL?cameraIndexCode=${params}&protocol=hls`
        )
        .then((res) => res.data);
};
// 获取流
export const connect=(params)=>{
    return axios1.get(
        `${base}/cam/operate?id=${params}&action=connect`
    )
}
export const disconnect=(params)=>{
    return axios1.get(
        `${base}/cam/operate?id=${params}&action=disconnect`
    )
}
//历史热力图
export const getTimePopulation = (params) => {
    return axios1
        .get(`${base}/wifi/timePopulation`, { params: params })
        .then((res) => res.data);
};
//实时热力图
export const getCurrentPopulation = (params) => {
    return axios1
        .get(`${base}/wifi/currentPopulation`, { params: params })
        .then((res) => res.data);
};

export default axios1;
