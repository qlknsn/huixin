import axios from '../index'
let base = '/api';

// 人群观测处理情况统计
export const peopleLookUpDealWith = params => {
    return axios.get(`${base}/suspect/faceRecognizeRecord/getNumBySolveStatus`).then(res => res.data)
};

//昨日各小时人流量
export const getYesterdayPeopleNumPerHour = params => {
    return axios.get(`${base}/wifi/hourlyTotalPopulation`).then(res => res.data)
};

// 人像库图像命中统计
export const peopleLibPercentAndMsgSure = params => {
    return axios.get(`${base}/suspect/faceRecognizeRecord/suspectHitStatistics`).then(res => res.data)
};

// 今日案件统计
export const todayAnJianStatistic = params => {
    return axios.get(`${base}/stateventcn/currentStatEventCN`).then(res => res.data)
};

