import axios from '../index'
let base = '/api';
export const getPortraitCategory  = params => {
    return axios.get(`${base}/suspect/portrait/portraitCategory`).then(res => res.data)
};

// 每个摄像头推送次数
export const getPushNumPerCam  = params => {
    return axios.get(`${base}/suspect/faceRecognizeRecord/pushNumPerCam`).then(res => res.data)
};

// 前10个识别准确率最高的摄像头
export const getPushAccuracyPerCam   = params => {
    return axios.get(`${base}/suspect/faceRecognizeRecord/pushAccuracyPerCam`).then(res => res.data)
};
export const getRecentlyWeekPushNum   = params => {
    return axios.get(`${base}/suspect/faceRecognizeRecord/recentlyWeekPushNum`).then(res => res.data)
};
export const getGetNewestRecords = params => {
    return axios.get(`${base}/suspect/faceRecognizeRecord/getNewestRecords`).then(res => res.data)
};

export const riverCamList = params => {//获取河道坐标
    return axios.get(`${base}/river/showInfo`,{params:params}).then(res => res.data)
};
export const riverSingleInfo = params => {//获取单个河道坐标信息
    return axios.get(`${base}/river/singleInfo`,{params:params}).then(res => res.data)
};

