import axios from '../index'

let base = '/api';

export const radarTrackHistoryStatistics = params => {
    return axios.get(`${base}/electrobike/radarTrackHistoryStatistics`).then(res => res.data)
};
export const lastTrackForBand = params => {
    return axios.get(`${base}/electrobike/lastTrackForBand`).then(res => res.data)
};
export const elderlyAndBandInfo = params => {
    return axios.get(`${base}/electrobike/elderlyAndBandInfo`,{params:params}).then(res => res.data)
};
export const fireChannelCongestionWarningArea = params => {
    return axios.get(`${base}/earthMagnetic/fireChannelCongestionWarningArea`,{params:params}).then(res => res.data)
};

