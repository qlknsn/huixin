import axios from '../index'

let base = '/api';
//昨日每小时
export const getCountByYesterdayHour = params => {
    return axios.get(`${base}/car/getCountByYesterdayHour`).then(res => res.data)
};
//高发记录
export const getCountByCam = params => {
    return axios.get(`${base}/car/getCountByCam`).then(res => res.data)
};
//月周天违章记录
export const getPercentageByPrevious = params => {
    return axios.get(`${base}/car/getPercentageByPrevious`, {params: params}).then(res => res.data)
};
//车牌分类
export const getPercentageByCarNumber = params => {
    return axios.get(`${base}/car/getPercentageByCarNumber`, {params: params}).then(res => res.data)
};
//最近一周
export const getCountByLastSevenDays = params => {
    return axios.get(`${base}/car/getCountByLastSevenDays`, {params: params}).then(res => res.data)
};
