import axios from 'axios'

export const getPeopleTrack  = params => {
    return axios.get(`/static/peopletrack.json`).then(res => res.data)
};