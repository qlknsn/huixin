import axios from '../index'

let base = '/api';

export const getCurrentStatEventCN = params => {
    return axios.get(`${base}/stateventcn/currentStatEventCN`).then(res => res.data)
};
export const getMainCategoryCountToday = params => {
    return axios.get(`${base}/commontask/mainCategoryCountToday`).then(res => res.data)
};
export const getInfoSourceCountToday = params => {
    return axios.get(`${base}/commontask/infoSourceCountToday`).then(res => res.data)
};
export const getRateByTime = params => {
    return axios.get(`${base}/commontask/rateByTime`).then(res => res.data)
};
