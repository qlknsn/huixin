import axios from 'axios'
let base = '/api';
export const locaVideo = params => {
    return axios.post(`${base}/video/screen`,params).then(res => res.data)
};
export const videoList = params => {
    return axios.get(`${base}/video/screens`).then(res => res.data)
};
export const updateVideo = params => {
    return axios.put(`${base}/video/screen`,params).then(res => res.data)
};
export const deleteVideo = params => {
    return axios.delete(`${base}/video/screen?id=${params.id}`).then(res => res.data)
};