import axios from '../index'
let base = '/api';

// 数量前十位手机品牌统计
export const phoneBrandStatistic = params => {
    return axios.get(`${base}/wifi/timeBrand`, {params: params}).then(res => res.data)
};

// 人流量嗅探 TOP 10
export const peopleNumXiuTan = params => {
    return axios.get(`${base}/wifi/topTenPopulation`, {params: params}).then(res => res.data)
};

// 昨日各小时人流量
export const yesterdayPeopleNumPerHour = params => {
    return axios.get(`${base}/wifi/hourlyTotalPopulation`).then(res => res.data)
};

// 昨日嗅探统计
export const yesterdayXiuTanAllNum = params => {
    return axios.get(`${base}/wifi/yesterdayTotalCount`, {params: params}).then(res => res.data)
};

// 一周内每天嗅探到的人数
export const lastWeekXiuTanPeopleNum = params => {
    return axios.get(`${base}/wifi/weeklyTotalPopulation`).then(res => res.data)
};
