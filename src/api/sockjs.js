import "../../static/js/sockjs";
import Stomp from "stompjs";

const base = SOCKET_ADDRESS;
// const base2 = "http://192.168.123.232:8091/ws/sockjs";

export default (name, back, arg) => {
    // let use=name.indexOf('消防')>=0?base:base;
    let use = base;

    let socket = new SockJS(use);
    let stompClient = Stomp.over(socket);
    // stompClient.debug = function (str) {
    //
    // }
    let promise = new Promise((resolve, reject) => {
        let connect = () => {
            return resolve(stompClient);
        };

        let error = () => {
            stompClient.disconnect();
            setTimeout(function () {
                back(arg);
            }, 120000);
            reject(name + "连接时发生了错误!");
        };

        stompClient.connect({}, connect, error);
    });
    return promise;
};
