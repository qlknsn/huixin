// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import "babel-polyfill";

import Vue from "vue";
import App from "./App";
import { router } from "./router";
import store from "./store";
import mixin from "./mixin";
// include animation styles
import "vodal/common.css";
import "vodal/rotate.css";

import Vodal from "vodal";

Vue.component(Vodal.name, Vodal);

import "iview/dist/styles/iview.css";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
Vue.use(ElementUI);

Vue.mixin(mixin);
Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
    el: "#app",
    router,
    store,
    template: "<App/>",
    components: { App },
});
