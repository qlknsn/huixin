import Vue from 'vue'
import Router from 'vue-router'
import login from '@/components/login'
import roadcams from '@/components/component/roadcams'
import main from '@/components/main'
import peopleCounting from '@/components/component/people-counting'
import telephone from '@/components/component/12345'
import parking from '@/components/component/parking'
import electrombile from '@/components/component/electrombile'
import index from "@/components/component/index"
import bracelet from "@/components/component/bracelet"
import {reqLogout} from "@/api/index";
//
Vue.use(Router)

export const router = new Router({
  routes: [
      {
          path: '/login',
          name: 'login',
          component: login
      },
      {
          path: '/',
          name: 'main',
          component: main,
          children: [
              {path: '/', name: '首页', component: index},
              {path: '/roadcams', name: '人群观测布控', component: roadcams},
              {path: '/peopleCounting', name: '人群密度', component: peopleCounting},
              {path: '/12345', name: '12345市民热线', component: telephone},
              {path: '/parking', name: '车辆违停', component: parking},
              {path: '/electrombile', name: '消防警报', component: electrombile},
              {path: '/bracelet', name: '手环车辆', component: bracelet},
          ]
      },

  ]
})

router.beforeEach((to, from, next) => {

    if (to.path === '/login') {
        localStorage.removeItem('user');
        localStorage.removeItem('mqTopicAddress');
        // reqLogout();
    }
    let user = localStorage.getItem('user')
    if (!user && to.path !== '/login') {
        next({path: '/login'})
    } else {
        next()
    }

})
