/**
 * Created by Administrator on 2017/7/10 0010.
 */


//室内地图

export default class overlay extends google.maps.OverlayView {
    constructor(map) {
        super();
        // Now initialize all properties.
        this.bounds_ = new google.maps.LatLngBounds(
            //左下角121.567746,31.113377
            new google.maps.LatLng(31.113466, 121.567690),
            //右上角121.571302,31.115291
            new google.maps.LatLng(31.115384, 121.571246));
        this.image_ = '/static/img/map/overlay.png';
        this.map_ = map;

        // Define a property to hold the image's div. We'll
        // actually create this div upon receipt of the onAdd()
        // method so we'll leave it null for now.
        this.div_ = null;

        // Explicitly call setMap on this overlay
        new Promise((resolve, reject) => {
            this.setMap(map);
            setTimeout(resolve, 500)

        }).then(() => {
            if (map.getZoom() >= 16) {
                this.show()
            }
            else {
                this.hide()
            }
        })

    }

    onAdd = () => {
        let div = document.createElement('div');
        div.style.border = 'none';
        div.style.borderWidth = '0px';
        div.style.position = 'absolute';

        // Create the img element and attach it to the div.
        let img = document.createElement('img');
        img.src = this.image_;
        img.style.width = '100%';
        img.style.height = '100%';
        div.appendChild(img);

        this.div_ = div;

        // Add the element to the "overlayImage" pane.
        let panes = this.getPanes();
        panes.overlayLayer.appendChild(this.div_);
    }

    draw = () => {
        let overlayProjection = this.getProjection();

        // Retrieve the south-west and north-east coordinates of this overlay
        // in LatLngs and convert them to pixel coordinates.
        // We'll use these coordinates to resize the div.
        let sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());
        let ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());

        // Resize the image's div to fit the indicated dimensions.
        let div = this.div_;
        div.style.left = sw.x + 'px';
        div.style.top = ne.y + 'px';
        div.style.width = (ne.x - sw.x) + 'px';
        div.style.height = (sw.y - ne.y) + 'px';
    }

    hide = () => {
        if (this.div_) {
            // The visibility property must be a string enclosed in quotes.
            this.div_.style.visibility = 'hidden';
        }
    };

    show = () => {
        if (this.div_) {
            this.div_.style.visibility = 'visible';
        }
    };

}
