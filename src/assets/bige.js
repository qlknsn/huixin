/**
 * Created by Administrator on 2017/7/4 0004.
 */

export const Bige = class {
    tileSize = new google.maps.Size(256, 256);
    //地图最大级别
    maxZoom = 18;
    //地图最小级别
    minZoom = 10;
    //地图名称
    name = "基本地图";
    //此地图种类获取URL的地址
    getTile = (coord, zoom, ownerDocument) => {
        let img = ownerDocument.createElement("img");
        img.style.width = this.tileSize.width + "px";
        img.style.height = this.tileSize.height + "px";
        // let strURL='http://mt0.google.cn/vt/lyrs=m@365000000&hl=zh-CN&gl=cn&x=' + coord.x + '&y=' +coord.y + '&z=' + zoom + '&s='+'Galileo'.substring(0,coord.x%8);
        let strURL = '/tiles/ditu/' + zoom + '/' + coord.x + '/' + coord.y + '.png';
        img.src = strURL;
        img.onerror = function () {
            this.src = 'images/error.png';
        };
        return img;
    };
};
//使用ImageMapType类创建一个地图
export const another_map = new google.maps.ImageMapType({
    tileSize: new google.maps.Size(256, 256),
    getTileUrl: function (coord, zoom) {
        //这里使用Bigemap地图下载器下载的离线瓦片作为此处的地址
        return '/tiles/weixing/' + zoom + '/' + coord.x + '/' + coord.y + '.png';
    },
    name: '卫星地图',
    minZoom: 10,
    maxZoom: 18
});
//使用ImageMapType类创建一个地图
export const heishe_map = new google.maps.ImageMapType({
    tileSize: new google.maps.Size(256, 256),
    getTileUrl: function (coord, zoom) {
        //这里使用Bigemap地图下载器下载的离线瓦片作为此处的地址
        return '/tiles/heise/' + zoom + '/' + coord.x + '/' + coord.y + '.png';
    },
    name: '黑色地图',
    minZoom: 10,
    maxZoom: 18
});

export const CenterControl = (controlDiv, map) => {

    // Set CSS for the control border.
    let controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.border = '2px solid #fff';
    controlUI.style.borderRadius = '3px';
    controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
    controlUI.style.cursor = 'pointer';
    controlUI.style.marginBottom = '0px';
    controlUI.style.marginTop = '10px';
    controlUI.style.textAlign = 'center';
    controlUI.title = 'Click to recenter the map';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    let controlText = document.createElement('input');
    controlText.style.color = 'rgb(25,25,25)';
    controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
    controlText.style.fontSize = '12px';
    controlText.style.lineHeight = '20px';
    controlText.style.paddingLeft = '5px';
    controlText.style.paddingRight = '5px';
    controlText.style.verticalAlign = 'top';

    controlUI.appendChild(controlText);

    let controlButton = document.createElement('button');
    controlButton.style.color = 'rgb(25,25,25)';
    controlButton.style.fontFamily = 'Roboto,Arial,sans-serif';
    controlButton.style.width = '26px';
    controlButton.style.height = '26px';
    controlButton.style.lineHeight = '26px';
    controlButton.style.padding = '0px';
    controlButton.innerHTML = '<img src="/static/img/map/search.png" height="22" width="22">';
    controlUI.appendChild(controlButton);

    controlText.addEventListener('keyup', function () {
        // console.log(controlText.value)

    })

}
