/**
 * Created by Administrator on 2017/7/5 0005.
 */
import Stomp from "stompjs";
import { reqLogout } from "../api/index";
export default {
    data() {
        return {
            MQusename: MQ_USERNAME,
            MQpassword: MQ_PASSWORD,
            clientMq: null,
        };
    },
    methods: {
        //登出
        handleLogOut() {
            this.show = true;
        },
        logOut() {
            reqLogout().then((data) => {
                if (data.errcode === 0) {
                    localStorage.removeItem("user");
                    this.$router.push("/login");
                }
            });
        },

        //地图
        delpolygon() {
            let name = this.$route.name;

            this.$refs[name].setpolynull();
        },
        setType() {
            this.$nextTick(() => {
                let name = this.$route.name;

                this.$refs[name].settype("polygon");
            });
        },

        //MQ
        Ws() {
            this.$nextTick(() => {
                this.clientMq = Stomp.client(WEBSOCKET_ADDRESS);
                // this.clientMq.debug = function (str) {
                //     // console.log(str)
                // }
                // SockJS does not support heart-beat: disable heart-beats
                this.clientMq.heartbeat.outgoing = 0;
                this.clientMq.heartbeat.incoming = 0;

                this.clientMq.connect(
                    this.MQusename,
                    this.MQpassword,
                    this.on_connect,
                    this.on_error,
                    "/"
                );
            });
        },
        on_connect() {
            // this.client.subscribe('/queue/test', (data) => {
            //     this.value = data.body
            // })
            // console.log('success!!')
        },
        on_error(error) {
            console.log("mq连接错误! 错误信息：", error);
            setTimeout(this.Ws, 120000);
        },
        //向mq发信息
        sendMQ(name, head, msg) {
            this.clientMq.send(name, { head }, msg);
        },
        //判断是否拥有主控权限
        isMainControl: function () {
            let permission = localStorage.getItem("userPermission");
            let perJson = JSON.parse(permission);
            for (let m = 0; m < perJson.length; m++) {
                if (perJson[m].permissionName == "mainControl:read") {
                    //主控权限
                    return true;
                }
            }
            return false;
        },
        //获取保存结合过uuid的mq地址
        getMqTopicAddress: function () {
            let mqTopicAddress = localStorage.getItem("mqTopicAddress");

            if (mqTopicAddress != null) {
                return mqTopicAddress;
            }
            return null;
        },
    },
};
