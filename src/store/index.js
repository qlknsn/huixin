/**
 * Created by Administrator on 2017/7/4 0004.
 */
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        agent: false,
        heatData: [],
        clientMq:{}
    },
    getters: {
        getAgent(state) {
            return state.agent
        },
        getClient(state){
          return state.clientMq
        },
        getHeatData(state) {
            return state.heatData
        },
    },
    mutations: {
        setAgent(state) {
            state.agent = !state.state
        },
        setClient(state,clientMq){
            state.clientMq=clientMq
        },
        setHeatData(state, heatData) {
            state.heatData = heatData
        },
    },
    actions: {}

})
